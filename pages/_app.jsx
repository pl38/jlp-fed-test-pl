import Layout from "../components/layout/layout";
import "../styles/globals.scss";

function _app({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default _app;
