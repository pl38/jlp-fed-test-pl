import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./product-detail.module.scss";
// import axios from "axios";
import Data from "../../mockData/data2.json";
import Head from "next/head";

// export async function getServerSideProps(context) {
//     const id = context.params.id;
//     // const response = await fetch(
//     //     "https://api.johnlewis.com/mobile-apps/api/v1/products/5561997"
//     // );
//     //
//     // const data =  await response.json();
//     //
//     return {
//         props: {data},
//     };
// }

// export async function getServerSideProps(context) {
//     const id = context.params.id;
//     const data = await axios.get('https://api.johnlewis.com/mobile-apps/api/v1/products/' + id)
//         .then(function (response) {
//             return response.data;
//         }).catch(err => console.error(err));
//     return {
//         props: {data: data},
//     }
// }

export function getServerSideProps(context) {
  const id = context.params.id;
  const data = Data.detailsData.find(({ productId }) => productId === id);
  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  let currencySymbol = "";
  const currency = data.price.currency;
  if (currency === "GBP") {
    currencySymbol = "£";
  }

  let attributes = data.details.features[0].attributes.sort((a, b) => {
    // shouldn't have 3 return points
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });

  return (
    <div>
      <Head>
        <title>
          JL &amp; Partners | {data.crumbs[0].displayName} |{" "}
          {data.crumbs[1].displayName}
        </title>
        <meta name="dishwasher" content="shopping" />
      </Head>
      <div className={styles.header}>
        <h1>{data.title}</h1>
      </div>
      <div className={styles.wrapper}>
        <div className={styles.content}>
          <div className={styles.panelLeft}>
            <ProductCarousel image={data.media.images.urls[0]} altText={data.media.images.altText}/>
            <div className={styles.showLarge}>
              <h1>
                {currencySymbol}
                {data.price.now}
              </h1>
              <h3 className={styles.fontNormal}>
                <div className={styles.specialOffer}>
                  {data.displaySpecialOffer}
                </div>
                <div>{data.additionalServices.includedServices}</div>
              </h3>
            </div>
            <div className={styles.informationGroup}>
              <h2 className={styles.fontNormal}>Product information</h2>
              <p>Product code: {data.productId}</p>
              <p
                dangerouslySetInnerHTML={{
                  __html: data.details.productInformation,
                }}
              />
            </div>
            <h2 className={styles.fontNormal}>Product specification</h2>
            <ul className={styles.productSpecListStyle}>
              {attributes.map((item) => (
                <li key={item.productId}>
                    <span>{item.name}</span>
                    <span className={styles.floatRight}>{item.value}</span>
                </li>
              ))}
            </ul>
          </div>
          <div
            className={[styles.panelRight, styles.showSmall, "main-class"].join(
              " "
            )}
          >
            <h1>
              {currencySymbol}
              {data.price.now}
            </h1>
            <h3 className={styles.fontNormal}>
              <div className={styles.specialOffer}>
                {data.displaySpecialOffer}
              </div>
              <div>{data.additionalServices.includedServices}</div>
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
