import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import Data from '../mockData/data.json';

// export async function getServerSideProps() {
//   const response = await fetch(
//     "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//   );
//   const data = await response.json();
//   return {
//     props: {
//       data: data,
//     },
//   };
// }

const Home = ({data = Data}) => {
    let items = data.products;
    return (
        <div>
            <Head>
                <title>JL &amp; Partners | Home</title>
                <meta name="dishwasher" content="shopping"/>
            </Head>
            <div>
                <h1>Dishwashers</h1>
                <div className={styles.content}>
                    {items.map((item) => (
                        <Link
                            key={item.productId}
                            href={{
                                pathname: "/product-detail/[id]",
                                query: {id: item.productId},
                            }}
                        >
                            <a className={styles.link} aria-roledescription="link" aria-label={item.title} data-testid={'test-' + item.productId}>
                                <ProductListItem price={item.price.now} image={item.image} title={item.title}
                                                 currency={item.price.currency}/>
                            </a>
                        </Link>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Home;
