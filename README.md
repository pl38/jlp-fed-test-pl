## Changes, updates and observations

#### NVM ([Node Virtual Machine](https://github.com/nvm-sh/nvm))
Now using node v14.20.0 (npm v6.14.17) - do not install npm > v6 as suggested. I ran an `npm audit fix` but did not do a full upgrade, keeping the packages within their minor version.

### General
Move from px units to REM where possible and define base font size as 16px.

Most of the issues I faced were related to tooling and package versioning, and I encountered errors around things such as

```
Configuration error:
    Could not locate module ../styles/globals.scss mapped as:
    /Volumes/projects/jlp-fed-test-main-pl/__mocks__/styleMock.js.
```

Also [Drop support for React 17 in testing-library/react 13](https://github.com/testing-library/react-testing-library/releases/tag/v13.0.0) the package json has `"@testing-library/react": "^13.1.1"` and `"react": "17.0.1"` .

I used vanilla CSS and .scss as provided, but I have moved to TailWind CSS is most of what I do, and I find it much more manageable.

### index.jsx - Home

Import data from `mockData/data.json`; this data set is not the exact data returned in the .api response.

Remove the inline product list code and use the ProductListItem component.

Change the prop  `description`  used by the ProductListItem component to `title` to correctly reflect the var used in the data.

#### index.module.scss
To better reflect the layout shown in `designs/product-grid.png`, remove the following CSS references:

```
.content {
  gap: 1px;
  padding: 1px;
  padding: 25px;
}

.link {
  display: block;
//  and change to:
  display: flex;
//  for <a> height issue
}
```

Also, remove unnecessary (duplicated or over-riding)

```
.price {
  font-weight: bold;
}
.content {
  background-color: #fff;
}

```


### product-list-item.jsx - ProductListItem

Change the prop  `description`  to `title` to correctly reflect the var used in the data.

Change the image alt tag to use the `title` content.

Add the `currencySymbol` var and check if GBP; the symbol is supplied in the .api response.


#### product-list-item-module.scss
To better reflect the layout shown in `designs/product-grid.png`, add the following CSS:
```
.content {
  border: 1px solid #dfdcd3;
}
```


### product-detail/[id].jsx - ProductDetail

Import data from `mockData/data2.json`; this data set is not the same as returned by the .api response.

It may be my firewall, but `export async function getServerSideProps(context)` is not responding for me. I wrote an async  [Axios ](https://axios-http.com) request, which is working, but as the data is not the same as the mockData, I'll continue to use the mockData2.json data.

Write `export function getStaticProps(context)` to return  ProductDetail props.

Add the `currencySymbol` var and check if GBP; the symbol is not supplied in the .api response.

Use `crumbs` to update the page title.

Refactor  `dangerouslySetInnerHTML` where appropriate.

Sort alphabetically  `data.details.features[0].attributes`  as per `designs/product-page-landscape.png`  and add key to map:
```
{attributes.map((item) => (
    <li key={item.productId}>....
```

The content block headed `Product information` is responsive. I have chosen not to implement this for the time being.

I have not implemented the carousel for the item page.

I have not implemented the tooltip available in the attributes.


### Tests

`App.test.js' are elementary tests but hopefully enough, to begin with.

The link test is not working for me, but I've left  it in:

```
it('can click to item', () => {
  const link = screen.getByTestId('test-1955287');
  expect (link).toBeInTheDocument();
  // fireEvent.click(link);*
})
```


The dynamic routing used by `ProductDetail` via NextJs is new to me, and although I'm sure I could work my way through it, I'll leaving it for now.
