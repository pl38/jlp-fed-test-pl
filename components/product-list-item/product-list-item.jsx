import styles from "./product-list-item.module.scss";

const ProductListItem = ({image, price, title, currency}) => {
    let currencySymbol = '';
    if (currency === 'GBP') {
        currencySymbol = '£';
    }
    return (
        <div className={styles.content}>
            <div>
                <img src={image} style={{width: "100%"}} alt={title}/>
            </div>
            <div>{title}</div>
            <div aria-label="Item Price" className={styles.price}>{currencySymbol}{price}</div>
        </div>
    );
};

export default ProductListItem;
