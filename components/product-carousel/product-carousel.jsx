import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ image, altText }) => {
  return (
    <div className={styles.productCarousel}>
      <img src={image} alt={altText} style={{ width: "100%", maxWidth: "500px" }} />
    </div>
  );
};

export default ProductCarousel;
