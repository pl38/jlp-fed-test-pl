import {createEvent, fireEvent, render, screen} from "@testing-library/react";
import Home from "../pages";

beforeEach(() => {
  render(<Home />);
})

it('has the title Dishwashers', () => {
  const titleElement = screen.getByText(/Dishwashers/i);
  expect(titleElement).toBeInTheDocument();
})

it('has the link to a product item', () => {
  expect(document.querySelector("a").getAttribute("href")).toContain(/product-detail/);
})

it('has at least 1 image with the alt containing bosch', () => {
  const image = screen.getAllByRole('img', {  name: /bosch/i});
  expect(image).toBeDefined();
})

it('can click to item', () => {
  const link = screen.getByTestId('test-1955287');
  expect(link).toBeInTheDocument();
  // fireEvent.click(link);
})
